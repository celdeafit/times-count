var express = require('express');
var router = express.Router();
var firebase = require("firebase");

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('add', { title: 'Add car' });
});

router.post('/', function(req, res, next) {
	var number		=	req.body.carNumber,
		name		=	req.body.carName,
		university	=	req.body.carUniversity,
		color		=	req.body.carColor
		laps		=	"0";

	firebase.database().ref('car/n' + number).set({
		number:			number,
		name:			name,
		university:		university,
		color:			color,
		laps:			laps
	});

	res.render('add', { title: 'Add car', status: 'Car added Successful!' });
});

module.exports = router;