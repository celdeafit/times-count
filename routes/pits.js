var express = require('express');
var router = express.Router();
var firebase = require('firebase');
var io = require('socket.io')();

/* GET home page. */
router.get('/', function(req, res, next) {
	var snap = "";
	
	firebase.database().ref('car/').on("value", function(snapshot) {
		snap = snapshot.val();
	}, function (errorObject) {
		console.log("The read failed: " + errorObject.code);
	});

	res.render('pits', { title: 'Pits count', cars: snap });
});

module.exports = router;