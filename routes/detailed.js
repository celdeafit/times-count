var express = require('express');
var router = express.Router();
var firebase = require("firebase");

/* GET home page. */
router.get('/', function(req, res, next) {
	res.render('detailed', { title: 'Car Detail' });
});

module.exports = router;