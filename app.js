require('node-monkey').start({host: "127.0.0.1", port:"50500"});

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var handlebars = require('handlebars');

var routes = require('./routes/index');
var users = require('./routes/users');
var add = require('./routes/add');
var laps = require('./routes/laps');
var pits = require('./routes/pits');
var detailed = require('./routes/detailed');

var app = express();

/*  Firebase  */
var firebase = require("firebase");
firebase.initializeApp({
  serviceAccount: {
    "projectId": "celd-timescount",
    "clientEmail": "serversideclient@celd-timescount.iam.gserviceaccount.com",
    "privateKey": "-----BEGIN PRIVATE KEY-----\nMIIEwAIBADANBgkqhkiG9w0BAQEFAASCBKowggSmAgEAAoIBAQDjTsnsw2jJcgRx\nPB+YSNeGwRAIfeTw9xNKSkjMOJ4Vej6I1weWLo7YsVgY5rskw58zytzmtp8V3j5i\nQG92rwUKdQiVrRGDUL/0oYJnv2HlLdOAztMH5etu5RaS+MfX3LlFNSmoV6jGV0CZ\n+gJ1f8uB254Nnu/8J543hHFTX6UuRmzszlWl7YpqALruaJElzFavFzI0x0SCIXji\n7n4F+vaK5kkf5vBdd1Xrf8UJSuCr6Fc1KJqoHtHZpTtdz0PpXEIAjmkUQRHPpzgi\n5A6xpQmB4FTJ8l22bet+ciMffwR8y5EXEmrWJXuHkWniGkoArzCSA6LMKr2ioy4X\nCMED7ns9AgMBAAECggEBANirk6LSKaJLwpxFWUCJvpe+lA/kIUTn30VjRb0VAusq\nGU3K2np3asZ3Vah211CV/70Y1DQ/Q6lOrVxuW1FgR4fOQx6ZWCY55J/FDE7kiOYy\nJneTrmjK6v5L9Jan+aZzyU6E/G5xEHnvVa7wN0PVSyE/PItzQwKyqQf2gSed9hAC\nWWKSP94MljEWD7lXVgZeeUIDB40XCtBRMTIVxDMTpbYS8FmnIrXp4+HxZkvLVtIh\nFn4nrOzJwPVSbFwCyKK+qpzHhpvx2Pb7v542XRSrWIAzp8Sp06OEblNlxsXNnb2v\n4tAwWNNS5rZGnGrzAQ1Mu30liKCSE6fz1MhkZsHJAAECgYEA+EiF31/u5FFLPf2f\ni8LrZ17l63Y7v7HiRI5ga87xJ0WdQ5n8b+3C0YuzemNSGDLUzcRxUhKD/XXjllT6\nm1g88Mbzry9nl9eobg7rQS83df2g5KSe9Cf5Enm03TBs+pCdmudSFXJ55PtHAD1q\nEO6cvnNr73W8PGAxN8NaWbuhOz0CgYEA6l9fhghw/1waYocLjugzKoj7HpE+XvFT\nXlAVkK7Y3Dzdh7impWVNUqqjVeV95iDGEPzdoLzq/KbiN3P848Bsho7oOKE9SGV1\nDeIVU9hFP1ig3xfFe2sIh+mJYDY1pubzuj28rIvW897LMUo1JxyvycA8nruM2oFE\n48dQFo9WQAECgYEAiKRBKSI+82DaLA+9Esn/jDBqESBM2b8oa9p/rlWB2r8xQdcp\nWuBba7+a0yx+LdsryuLmCuktwvxyeHXkRkJVp52W1z8boDGL/KYxSvqVZ1ZiifMf\nl/e32DcrFuBj2bJlSq8sOal69E5mUPd7vD3V/eq/6FW/durpq2rT+raMkXECgYEA\nsOkO1MB4cvjV3btsKRHy1U+Spk8iUk49DLrMnGquvQegfHsQJ1ahGB4/rm3NK7Ff\ncKE+tb8EbXOW3B7MLCoqgSNL/LsQ5B3ZToJycNiV5KwEuoFaCsAPL3BDxwEEXJiG\nLvW8/k2MEocAv5LxLjXEJzjZSUPunVzoYDspo+3MQAECgYEAkXABbKQk2NijGSdH\n155EJ7+2AAKS8WUR2M3bWRwxphVVgrxQ+maSIisY3gvJHY2/P/di6cQQFRCCfZ/o\n98DkcwyplYJ2N3zK2G5N0ggx5fpcxhGBBcR26HCYKl3YW0bm4Ydrd7AWpaL6E5m6\ncCS4k9FhchoyKcarTMn5WyhTVX0=\n-----END PRIVATE KEY-----\n"
  },
  databaseURL: "https://celd-timescount.firebaseio.com"
});
/*  Firebase end  */

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', routes);
app.use('/users', users);
app.use('/add', add);
app.use('/laps', laps);
app.use('/pits', pits);
app.use('/detailed', detailed);
app.use('/cambria', detailed);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// error handlers

// development error handler
// will print stacktrace
if (app.get('env') === 'development') {
  app.use(function(err, req, res, next) {
    res.status(err.status || 500);
    res.render('error', {
      message: err.message,
      error: err
    });
  });
}

// production error handler
// no stacktraces leaked to user
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  res.render('error', {
    message: err.message,
    error: {}
  });
});


module.exports = app;
