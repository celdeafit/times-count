function addCar(number, name, university, color) {
	firebase.database().ref('car/' + number).set({
		number:			number,
		name:			name,
		university:		university,
		color:			color
	});
}